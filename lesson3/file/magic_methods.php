<?php

class Product
{
    private $collection = [];

    public function __get($key)
    {
        if (!isset($this->collection[$key])) {
            return null;
        }
        return $this->collection[$key];
    }

    public function __set($propertyName, $value)
    {
        $this->collection[$propertyName] = $value;
    }

    public function __isset($propertyName)
    {
        return isset($this->collection[$propertyName]);
    }

}

$product = new Product();
$product->name = 'T-Shirt';
$product->description = 'Some beatusdfuigsdfuvhuidsfbv';

echo (int) isset($product->name);
die;
var_dump($product);
