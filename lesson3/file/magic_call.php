<?php

class User
{
    private $data = [];

    public function __call($methodName, $arguments)
    {
//        switch ($methodName) {
//            case 'getEmail': return 'vasia@pupkin.com';
//            case 'getUserName': return 'Vasia';
//
//        }
        if ($methodName == 'setName' && !empty($arguments)) {
            $this->name = $arguments[0];
        }
    }

    public function __set($key, $value)
    {
        $this->data[$key] = $value;
    }
}

$user = new User();
$user->setName('vasia');

var_dump($user);
//echo $user->getEmail() . PHP_EOL;
//echo $user->getUserName();
