<?php

class Registry
{
    private $store = [];

    private static $instance;


    private function __construct(){}
    private function __clone(){}

    /**
     * @return Registry
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __get($key)
    {
        if (!isset($this->store[$key])) {
            return null;
        }
        return $this->store[$key];
    }

    public function __set($key, $value)
    {
        $this->store[$key] = $value;
    }
}

$registry = Registry::getInstance();
$registry->db = [
    'host' => '127.0.0.1',
    'dbname' => 'test'
];
$container = new stdClass();
$container->value = 'test';
$container->class = Registry::class;

$registry->container = $container;

var_dump($registry->db);