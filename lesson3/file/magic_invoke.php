<?php

class GetFileContent
{
    public function __invoke($file, $index)
    {
        $files = glob($file);
        return file_get_contents($files[$index]);
    }
}

class DumpContent
{
    public function __invoke()
    {
        $range = range(0, 100);
        $arrayContent = var_export($range, true);

        file_put_contents(
            'range.php',
            $this->startPhpFile($arrayContent)
        );
    }

    private function startPhpFile($content)
    {
        return sprintf('<?php return %s;', $content);
    }
}

$phpArrayContent = new DumpContent();
$phpArrayContent();

//
//$getFileContent = new GetFileContent();
//echo $getFileContent('*.php', 0);