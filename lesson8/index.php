<?php



class ClientModel
{

    private $db;
    private $user = 'root';
    private $pass = 'root';

    public $from;
    public $where;
    public $execute;
    public $stmt;

    function __construct()
    {
        try {
            $this->db = new PDO('mysql:host=localhost;dbname=hillel_test', $this->user, $this->pass);
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }

    }

    public function from($from = 'SELECT * FROM clients')
    {
        $this->from = $from;
        return $this;
    }

    public function where($where)
    {
        $this->where = ' WHERE ' . $where;
        return $this;
    }

    public function execute()
    {
        $res = $this->from . $this->where;
        $this->stmt = $this->db->prepare($res);
        $this->stmt->execute();

        return $this;
    }

    public function show()
    {
        $result = [];

        foreach ($rows = $this->stmt->fetchAll() as $row)
        {
            $result[] = $row;
        }

        return $result;
    }

    public function clientAgeUpdate($id, $age)
    {
        $this->db->prepare("UPDATE clients SET age = $age  WHERE id = $id")->execute();
    }

    public function clientNameUpdate($id, $name)
    {
        $this->db->prepare("UPDATE clients SET name = $name WHERE id = $id")->execute();
    }

    public function clientNotActive($id)
    {
        $this->db->prepare("UPDATE clients SET is_active = 0  WHERE id = $id")->execute();
    }

    public function clientDeleteNotActive()
    {
        $this->db->prepare("DELETE FROM clients WHERE is_active = 0")->execute();
    }

    public function clientDeleteAll()
    {
        $this->db->prepare("DELETE FROM clients")->execute();
    }

}


$client = new ClientModel();
echo '<pre>';
//print_r($client->from()->execute()->show());
//print_r($client->from()->where('is_active = 1')->execute()->show());
//print_r($client->from()->where('age >= 30')->execute()->show());
//print_r($client->from()->where('name LIKE \'%В%\'')->execute()->show());
//print_r($client->from("SELECT COUNT(id) FROM clients")->execute()->show());
//print_r($client->from("SELECT max(age) FROM clients")->execute()->show());
//print_r($client->from("SELECT count(id) FROM clients")->where('is_active = 1')->execute()->show());
//print_r($client->from()->where('ORDER BY age ASC')->execute()->show());
//print_r($client->from()->where('ORDER BY name ASC')->execute()->show());
//print_r($client->from("SELECT count(id) FROM clients")->where('is_active = 1 AND age > 25')->execute()->show());

//$client->clientAgeUpdate(7, 50);
//$client->clientNameUpdate(7, 'Петя');
//$client->clientNotActive(7);
//$client->clientDeleteNotActive();
//$client->clientDeleteAll();
