<?php


namespace QuizProcessing;

use Exception;
use QuizProcessing\Struct\AddressInterface;
use QuizProcessing\Struct\ContactInterface;
use QuizProcessing\Struct\Factory\StructFactory;
use QuizProcessing\Struct\ProfileInterface;
use QuizProcessing\Struct\Teacher;
use QuizProcessing\Struct\User;

class DataParser
{
    private $data;
    private $jsonFile;

    public function __construct(string $jsonFile)
    {
        if (!file_exists($jsonFile)) {
            throw new Exception('File not exists');
        }
        $this->jsonFile = $jsonFile;
    }

    public function process()
    {
        try {
            $this->loadJson();

            foreach ($this->data as $item) {
                $struct = StructFactory::create($item['type']);
                if ($struct instanceof ContactInterface) {
                    $struct->name = $item['name'];
                    $struct->email = $item['email'];
                }

                if ($struct instanceof AddressInterface) {
                    $struct->address = $item['address'];
                }

                if ($struct instanceof ProfileInterface) {
                    $struct->firstName = $item['first_name'];
                    $struct->lastName = $item['last_name'];
                }

                if ($struct instanceof User)
                {
                    $struct->group = $item['group'];
                    $struct->average_score = $item['average_score'];
                }

                if ($struct instanceof Teacher)
                {
                    $struct->comment = $item['comment'];
                    $struct->salary = $item['salary'];
                }

                echo '<pre>';
                print_r($struct);
            }
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }

    private function loadJson()
    {
        $json = file_get_contents($this->jsonFile);

        $data = json_decode($json, true);
        if ($data === null) {
            throw new Exception('Invalid json file');
        }
        $this->data = $data;
    }
}