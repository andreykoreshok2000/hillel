<?php

class Mailer
{
    const NAME = 'name';

    private static $subject;

    public static function send($message, $recipient, $subject = null)
    {
        if ($subject) {
            self::$subject = $subject;
        }
        return sprintf('Sent %s to %s <%s>', $message, $recipient, self::$subject);
    }
}

echo Mailer::send('test message', 'abc@mail.com');