<?php

class UserProfile
{
    private $users = [];

    public function __construct($users)
    {
        $this->users = $users;
    }

    /**
     * 
     */
    public static function create($users)
    {
        return new self($users);
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        $this->sortList();

        return $this->users;        
    }

    /**
     * @param string $user2
     */
    public function addUser($user)
    {
        $this->users[] = $user;
    }

    private function sortList()
    {
        sort($this->users);
    }
}

$userProfile = UserProfile::create(
    ['Vasia', 'Petya', 'Masha']
);

var_dump($userProfile);die;
var_dump($userProfile->getUsers());
