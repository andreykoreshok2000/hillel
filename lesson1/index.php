<?php 

/**
 * 
 */
class UserProfile
{
	private $user = [];

	public function __construct($user)
	{
		$this->user[] = $user;
	}

	public static function create($user)
	{
		return new self($user);
	}

	public function addUser($user)
	{
		$this->user[] = $user;
	}
	private function sortList()
	{
		sort($this->user);
	}
	/**
	* @params string $user
	**/
	public function getUsers()
	{
		$this->sortList();
		return $this->user;
	}
}

/**
 * 
 */
class Mailer
{
	private static $subject;

	public static function send($message, $recipient, $subject=null) 
	{
		if ($subject) {
			self::$subject=$subject; 
		}
		return sprintf('Send %s to %s <%s>', $message, $recipient, self::$subject);
	}
}


// $userProfile = new userProfile(['Vasia', 'Petea', 'Oleg']);
var_dump(userProfile::create('test'));
// echo Mailer::send('test message', 'test', 'subject');
// var_dump($userProfile->getUsers());
//$userProfile->sortList();
// var_dump($userProfile->getUsers());

?>