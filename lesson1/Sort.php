<?php

class Sort
{
    public $arr = [];

    public function __construct($arr)
    {
        $this->setArr($arr);//die;
        $this->sort();
    }

    public function setArr($arr)
    {
        $this->arr = $arr;
    }

    public function sort()
    {
        if (!empty($this->arr) && is_array($this->arr)){
            sort($this->arr);
        }
    }

    public function getArr()
    {
        return $this->arr;
    }

}

$a = new Sort([5,3,2,4,1]);
var_dump($a->getArr());
$a->sort();
var_dump($a->getArr());
