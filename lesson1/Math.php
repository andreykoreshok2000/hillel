<?php

class Math
{
    /**
     * @param $number
     * @return int
     */
    public function calcFactorial($number)
    {
        if (!is_numeric($number))
        {
            return 'значения должны быть числами';
        }

        $fact = $number;
        $ffact = 1;

        while($fact >= 1)
        {
            $ffact = $fact * $ffact;
            $fact--;
        }

        return $ffact;
    }

    /**
     * @param $number1
     * @param $number2
     * @param $act
     * @return float|int|string
     */
    public function calc($number1, $number2, $act)
    {

        if (!is_numeric($number1) || !is_numeric($number2))
        {
            return 'значения должны быть числами';
        }

        switch($act){
            case '+':
                $res = $number1 + $number2;
                break;
            case '-':
                $res = $number1 - $number2;
                break;
            case '*':
                $res = $number1 * $number2;
                break;
            case '/':
                if( $number2 == '0')
                    return "На ноль делить нельзя!";
                else
                    $res = $number1 / $number2;
                break;
            default:
                echo "что то пошло не так";
        }

        return $res;
    }

}

$a = new Math();
var_dump($a->calcFactorial(5));
var_dump($a->calc(5,5,'+'));
