<?php

class LunaAlgorithm
{
    public $number;
    public $sum = 0;
    const TWO = 2;
    const NINE = 9;
    const ZERO = 0;

    public function __construct($digit)
    {
        $this->number = strrev(preg_replace('/[^\d]+/', '', $digit));
    }

    public function LunaAlgorithm()
    {

        for ($i = 0, $j = strlen($this->number); $i < $j; $i++)
        {
            if (($i % self::TWO) == self::ZERO) {
                $val = $this->number[$i];
            } else{
                $val = $this->number[$i] * self::TWO;
                if ($val > self::NINE) {
                    $val -= self::NINE;
                }
            }
            $this->sum += $val;
        }
        return (($this->sum % 10) === self::ZERO);
    }
}

$a = new LunaAlgorithm('4561 2612 1234 5467');
//$a = new LunaAlgorithm('4561 2612 1234 5464');

var_dump($a->LunaAlgorithm());