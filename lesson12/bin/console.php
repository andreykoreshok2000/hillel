<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Documentor\Command\FileInfoCommand;
use Documentor\Service\CsvRender;
use Documentor\Service\FileInfoService;
use Documentor\Service\TwigRender;
use Documentor\Service\xmlRender;
use Symfony\Component\Console\Application;
use Documentor\Command\VersionCommand;


$application = new Application();
$application->setName('hillel Documentor');

$fileInfoService = new FileInfoService();

$twigRender = new TwigRender(__DIR__ . '/../templates', __DIR__ . '/../reports/');
$csvRender = new CsvRender(__DIR__ . '/../reports/csv/');
$xmlRender = new XmlRender(__DIR__ . '/../reports/xml/');

$application->addCommands([
    new FileInfoCommand($fileInfoService, $twigRender, $csvRender, $xmlRender),
    new VersionCommand()
 ]);
$application->run();
