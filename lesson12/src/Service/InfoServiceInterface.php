<?php

namespace Documentor\Service;


interface InfoServiceInterface
{
    public function setFileName(string $string);
    public function getInfo(): array;
}