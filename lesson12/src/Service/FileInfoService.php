<?php

namespace Documentor\Service;

use Symfony\Component\Console\Exception\RuntimeException;

class FileInfoService implements InfoServiceInterface
{
    // php console.php doc:report Money.php
    /**
     * @var string
     */
    private $fileName;

    public function setFileName(string $fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    public function getInfo(): array
    {
        $reflectionClass = $this->getClass();

        $classMetaData = $this->getMetaData($reflectionClass->getDocComment());
        $methods = $this->getMethods($reflectionClass->getMethods());

        $info = [
            'class_name' => $reflectionClass->getName(),
            'class_data' => $classMetaData,
            'methods' => $methods,
        ];

        return $info;
    }

    /**
     * @param \ReflectionMethod[] $methods
     * @return array
     */
    private function getMethods(array $methods): array
    {
        $data = [];

        foreach ($methods as $method)
        {
            $data[] = [
                'meta' => $this->getMetaData($method->getDocComment()),
                'name' => $method->getName(),
                'arguments' => $this->getParametersData($method->getParameters()),
                'return_type' => (string) $method->getReturnType()
            ];
        }

        return $data;
    }

    /**
     * @param \ReflectionParameter[] $parameters
     */
    private function getParametersData($parameters): array
    {
        $data = [];

        foreach ($parameters as $parameter)
        {
            $data[] = [
                'name' => $parameter->getName(),
                'type' => (string) $parameter->getType(),
            ];
        }

        return $data;
    }

    private function getMetaData(string $comment): array
    {

        $regexp = '/@([a-z]+?)\s+(.*?)\n/mi';
        preg_match_all($regexp, $comment, $matches);

        $fields = [];
        $values = [];

        foreach ($matches[1] as $field)
        {
            $fields[] = $field;
        }

        foreach ($matches[2] as $value)
        {
            $values[] = $value;
        }

        $combined = array_combine($fields, $values);
        return $combined;
    }

    private function getClass(): \ReflectionClass
    {
        // костыль так делать не нужно
        //"E:\\OpenServer\\domains\\hillel.loc\\lesson12\\".$this->fileName
        $fileName = "E:\\OpenServer\\domains\\hillel.loc\\lesson12\\data\\".$this->fileName;

        if (!file_exists($fileName)) {
            throw new \RuntimeException('file not found');
        }

        include $fileName;

        // такой \ для винды
        $classPath = explode("\\", $fileName);
        if (empty($classPath)) {
            throw new RuntimeException('Invalid path');
        }
        $classPath = str_replace('.php', '', array_pop($classPath));

        return new \ReflectionClass($classPath);
    }

}