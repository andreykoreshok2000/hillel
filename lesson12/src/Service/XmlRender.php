<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 17.01.2021
 * Time: 19:47
 */

namespace Documentor\Service;


class xmlRender implements RenderInterface
{
    private $reportsDir;

    public function __construct(string $reportsDir)
    {
        $this->reportsDir = $reportsDir;

        if (!file_exists($this->reportsDir)) {
            mkdir($this->reportsDir, 0777);
        }
    }

    public function render(array $data)
    {
        $dom = new \DomDocument('1.0');
        $dom->formatOutput = true;

        $report = $dom->appendChild($dom->createElement('report'));

        $className = $report->appendChild($dom->createElement('className'));
        $className->appendChild($dom->createTextNode($data['class_name']));

        $classInfo = $className->appendChild($dom->createElement('classInfo'));

        foreach ($data['class_data'] as $item)
        {
            $classInfo->appendChild($dom->createTextNode($item));
        }

        foreach ($data['methods'] as $item)
        {
            $method = $className->appendChild($dom->createElement('method'));
            $method->appendChild($dom->createTextNode($item['name']));

            foreach ($item['meta'] as $meta)
            {
                $params = $className->appendChild($dom->createElement('params'));
                $params->appendChild($dom->createTextNode($meta));
            }

            foreach ($item['arguments'] as $argument)
            {
                $name = $className->appendChild($dom->createElement('name'));
                $name->appendChild($dom->createTextNode($argument['name']));
                $type = $className->appendChild($dom->createElement('type'));
                $type->appendChild($dom->createTextNode($argument['type']));
            }

            $return_type = $className->appendChild($dom->createElement('return_type'));
            $return_type->appendChild($dom->createTextNode($item['return_type']));

        }


        $fileName = $this->reportsDir . time() . 'file.xml';
        $dom->save($fileName);

    }

}