<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 04.01.2021
 * Time: 22:00
 */

namespace Documentor\Service;


class CsvRender implements renderInterface
{
    private $reportsDir;

    public function __construct(string $reportsDir)
    {
        $this->reportsDir = $reportsDir;

        if (!file_exists($this->reportsDir)) {
            mkdir($this->reportsDir, 0777);
        }
    }

    public function render(array $data)
    {
        $fp = fopen($this->reportsDir . time() . 'file.csv', 'w');

        $res = '';

        $res .= 'class name' . "\n" . "\t" . $data['class_name'] . "\n";
        $res .= 'class info';

        foreach ($data['class_data'] as $item)
        {
            $res .= "\n\t" . $item;
        }

        foreach ($data['methods'] as $item)
        {
            $res .= 'method ' . "\n\t\t" . $item['name'] . "\n";

            foreach ($item['meta'] as $meta)
            {
                $res .= "\t" . 'params ' . "\n\t\t" . $meta . "\n";
            }

            foreach ($item['arguments'] as $argument)
            {
                $res .= "\t" . 'name ' . "\n\t\t" . $argument['name'] . "\n";
                $res .= "\t" . 'type ' . "\n\t\t" . $argument['type'] . "\n";
            }

            $res .= "\t" . 'return_type ' . "\n\t\t" . $item['return_type']. "\n";

        }


        fputcsv($fp, [$res]);

        fclose($fp);
    }

}