<?php
/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 26.12.2020
 * Time: 19:32
 */

namespace Documentor\Service;


use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class TwigRender implements renderInterface
{
    private $twig;
    private $reportsDir;

    public function __construct(string $templatesDir, $reportsDir)
    {
        $loader = new FilesystemLoader($templatesDir);
        $this->twig = new Environment($loader, [
            'debug' => true
        ]);
        $this->reportsDir = $reportsDir;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function render(array $data)
    {
        $html = $this->twig->render('report.html.twig', $data);

        file_put_contents($this->reportsDir . time() . '.html', $html);
    }

}