<?php


namespace Documentor\Service;


class DataCollector
{
    public $fileName;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
    }

    public function collectData()
    {

        $fileContent = file_get_contents("E:\\OpenServer\\domains\\hillel.loc\\lesson12\\".$this->fileName);
        $tokens = token_get_all($fileContent);

        $ClassName = null;
        foreach ($tokens as $key => $token) {
            if (is_array($token)) {
                if (token_name($token[0]) == 'T_CLASS') {
                    $className = $tokens[$key+2][1];
                }
            }
        }

        include "E:\\OpenServer\\domains\\hillel.loc\\lesson12\\".$this->fileName;

        $reflectionClass = new \ReflectionClass($className);
        $methods = $this->getMethods($reflectionClass);

        // getfields
        // getAuthors
        // getCopyRight
    }

    private function getMethods(\ReflectionClass $class): array
    {
        $metaData = [];

        $methods = $class->getMethods();

        /**
         * \ReflectionClass
         */
        foreach ($methods as $method) {
            $metaData[] = [
                'name' => $method->getName(),
                'docBlock' => $method->getDocComment(),
                'returns' => (string) $method->getReturnType(),
                'accessLevel' => $this->formatModifiers($method->getModifiers()),
            ];
        }

        var_dump($metaData);
    }

    private function formatModifiers(int $modifiers): string
    {
        switch ($modifiers) {
            case \ReflectionMethod::IS_PUBLIC:
                return 'public';
            case \ReflectionMethod::IS_PROTECTED:
                return 'protected';
            case \ReflectionMethod::IS_PRIVATE:
                return 'private';
            default:
                return 'undetected';
        }
    }

    private function getClassAuthor(\ReflectionClass $class): ?string
    {
        $docComment = $class->getDocComment();
        if (!$docComment) {
            return null;
        }
        $regexp = '/@Author\s/m';
        preg_match_all($regexp, $docComment, $matches);
        var_dump($matches);
    }
}