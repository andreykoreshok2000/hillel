<?php

namespace Documentor\Service;


interface renderInterface
{
    /**
     * Render report data.
     *
     * @param array $data
     * @return string
     */
    public function render(array $data);
}