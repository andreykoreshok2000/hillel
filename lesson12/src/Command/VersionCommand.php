<?php


namespace Documentor\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VersionCommand extends Command
{
    // вызов команды php lesson12/bin/console.php version:info
    public function configure()
    {
        $this->setName('version:info')->setDescription('show application version');
    }
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $composerFile = __DIR__ . '/../../composer.json';

        if(!file_exists($composerFile)){
            $output->writeln('<error>composer.json file not found</error>');
            exit();
        }
        $jsonDate = file_get_contents($composerFile);

        $data = json_decode($jsonDate, true);

        if (!isset($data['version'])){
            $output->writeln('<error>version information not found</error>');
            exit(0);
        }
        $output->writeln('Application version '. $data['version']);
        exit(0);
    }
}