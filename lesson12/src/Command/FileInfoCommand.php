<?php

namespace Documentor\Command;


use Documentor\Service\InfoServiceInterface;
use Documentor\Service\renderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class FileInfoCommand extends Command
{
    /**
     * @var bool
     */
    private $dryRun = false;

    /**
     * @var InfoServiceInterface
     */
    private $infoService;

    private $render;
    private $csvRender;
    private $xmlRender;

    public function __construct(InfoServiceInterface $infoService,
                                RenderInterface $render,
                                RenderInterface $csvRender,
                                RenderInterface $xmlRender)
    {
//        php console.php doc:report Money.php
        $this->infoService = $infoService;
        $this->render = $render;
        $this->csvRender = $csvRender;
        $this->xmlRender = $xmlRender;
        parent::__construct('doc:report');
    }

    public function configure()
    {
        $this->setDescription('Read source file and prepare report')
            ->addArgument('filename', InputArgument::REQUIRED, 'Source file name')
            ->addOption('dry-run');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('filename');
        $output->writeln('filename: ' . $filename);

        if ($input->getOption('dry-run')){
            $output->writeln('Dry run checked');
            $this->dryRun = true;
        }

        $this->infoService->setFileName($filename);

        $info = $this->infoService->getInfo();

        $this->render->render($info);
        $this->csvRender->render($info);
        $this->xmlRender->render($info);

        exit(0);
    }

}