<?php

abstract class User
{
    protected $name;
    protected $age;

    abstract protected function getRole();
}

trait InfoUserTrait {

    private function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    private function setAge($age)
    {
        $this->age = $age;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getRole()
    {
        return strtolower(__CLASS__);
    }
}

class Admin extends User
{
    use InfoUserTrait;

    public function __construct($infoUser)
    {
        $this->setName($infoUser['name']);
        $this->setAge($infoUser['age']);
    }

}

class Viewer extends User
{
    use InfoUserTrait;
    public function __construct($infoUser)
    {
        $this->setName($infoUser['name']);
        $this->setAge($infoUser['age']);
    }

}

class Moderator extends User
{
    use InfoUserTrait;

    public function __construct($infoUser)
    {
        $this->setName($infoUser['name']);
        $this->setAge($infoUser['age']);
    }
}


$admin = new Admin(['name' => 'asd', 'age' => 20]);
echo $admin->getName() . PHP_EOL;
echo $admin->getAge(). PHP_EOL;
echo $admin->getRole(). PHP_EOL;

$viewer = new Viewer(['name' => 'qwerty', 'age' => 25]);
echo $viewer->getName() . PHP_EOL;
echo $viewer->getAge(). PHP_EOL;
echo $viewer->getRole(). PHP_EOL;

$moderator = new Moderator(['name' => 'zcv', 'age' => 30]);
echo $moderator->getName() . PHP_EOL;
echo $moderator->getAge(). PHP_EOL;
echo $moderator->getRole(). PHP_EOL;