<?php

interface Wheel
{
    public function getWheelCount();
}

interface Door
{
    public function getDoorsCount();
}

interface MotorType
{
    public function getMotorType();
}

interface Сaterpillar
{
    public function getСaterpillar();
}

interface fire
{
    public function fire();
}

interface headlights
{
    public function getCountHeadlights();
}

class Speed
{
    public function accelerate($speed,  $unit = 'миль в час')
    {
        return $speed . $unit;
    }

    public function decelerate($speed, $unit = 'миль в час')
    {
        return $speed . $unit;
    }
}

class Car extends Speed implements Wheel, Door, MotorType, headlights
{
    public function getWheelCount()
    {
        return 4;
    }

    public function getDoorsCount()
    {
        return 4;
    }

    public function getMotorType()
    {
        return 'Двигатель внутреннего сгорания';
    }

    public function getCountHeadlights()
    {
        return 6;
    }
}

class Bike extends Speed implements Wheel, MotorType, headlights
{
    public function getWheelCount()
    {
        return 2;
    }

    public function getMotorType()
    {
        return 'Сила тела';
    }

    public function getCountHeadlights()
    {
        return 1;
    }

}

class Tank extends Speed implements Сaterpillar, MotorType, Fire, headlights
{
    public function getСaterpillar()
    {
        return 2;
    }

    public function getMotorType()
    {
        return 'Паровой двигатель';
    }

    public function fire()
    {
        return 'Количество выстрелов в минуту 10';
    }

    public function getCountHeadlights()
    {
        return 2;
    }
}

$car = new Car();
echo $car->getWheelCount(); //вернет количество колес
echo $car->getDoorsCount(); // вернет количество дверей
echo $car->getMotorType(); // вернет тип двигателя: мотор или мускулы
echo $car->accelerate(10); // установить скорость 10 км/ч
echo $car->decelerate(5); // уменьшить скорость до 5 км/ч
echo $car->getCountHeadlights() . PHP_EOL;// количество фар

$bike = new Bike();
echo $bike->getWheelCount(); //вернет количество колес
echo $bike->getMotorType(); // вернет тип двигателя: мотор или мускулы
echo $bike->accelerate(10); // установить скорость 10 км/ч
echo $bike->decelerate(5); // уменьшить скорость до 5 км/ч
echo $bike->getCountHeadlights() . PHP_EOL;// количество фар

$tank = new Tank();
echo $tank->getСaterpillar(); //вернет количество траков гусеницы
echo $tank->getMotorType(); // вернет тип двигателя: мотор или мускулы
echo $tank->accelerate(10, 'км в час'); // установить скорость 10 км/ч
echo $tank->decelerate(5, 'км в час'); // уменьшить скорость до 5 км/ч
echo $tank->fire();
echo $tank->getCountHeadlights();// количество фар