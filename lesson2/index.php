<?php

class BaseAccount
{
    public $role;
    public $department;

    public function __construct($role, $department)
    {
        $this->role = $role;
        $this->department = $department;
    }

    public function GetDepartment()
    {
        return $this->department;
    }

    public function changeDepartment($newDepartment)
    {
        $this->department = $newDepartment;
    }

    public function getRole()
    {
        return $this->role;

    }
}

class UserAccount extends BaseAccount
{
    public function __construct($role)
    {
        $department = $role . 's';
        parent::__construct($role, $department);
    }

}

$a = new UserAccount('user');

print_r($a);