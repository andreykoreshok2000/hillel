<?php

interface Figure
{

}

interface Primitives
{

}


class Box implements Figure
{
    public $type = 'figure';
}

class Circle implements Figure
{

}

class Triangle implements Figure
{

}

class Dot implements Primitives
{

}

class Line implements Primitives
{

}

$mixed = [
    new Circle(),
    new Dot(),
    new Triangle(),
    new Box(),
    new Line()
];

$figure = [];
$primitives = [];

/** @var Box $item */
foreach ($mixed as $item)
{
    if ($item->type instanceof Figure) {
        $figure[] = $item;
    } elseif ($item instanceof Primitives) {
        $primitives[] = $item;
    }
}

var_dump($primitives);
var_dump($figure);