<?php

class BaseAccount
{
    protected $role;
    protected $department;

    public function __construct($role, $department)
    {
        $this->role = $role;
        $this->department = $department;
    }

    public function getDepartment()
    {
        return $this->department;
    }

    public function changeDepartment($newDepartment)
    {
        $this->department = $newDepartment;
    }

    public function getRole()
    {
        return $this->role;
    }
}

class UserAccount extends BaseAccount
{
    public function __construct($role)
    {
        $department = $role . 's';
        parent::__construct($role, $department);
    }

    public function getDepartment()
    {
        return strtoupper($this->department);
    }
}

class AdminAccount extends BaseAccount
{
    public function __construct($role)
    {
        $department = $role . 's';
        parent::__construct($role, $department);
    }

    public function getRole()
    {
        return strrev($this->role);
    }
}

$userAccount = new UserAccount('user');

echo sprintf('User role: %s, department:%s',
    $userAccount->getRole(),
    $userAccount->getDepartment()
) . PHP_EOL;

$adminAccount = new AdminAccount('admin');

echo sprintf('User role: %s, department:%s',
    $adminAccount->getRole(),
    $adminAccount->getDepartment()
);