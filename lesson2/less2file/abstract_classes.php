<?php


abstract class Figure
{
    protected $dimensions;

    abstract protected function getSquare();
}

class Circle extends Figure
{
    public function __construct($radius)
    {
        $this->dimensions['radius'] = $radius;
    }

    public function getSquare()
    {
        return M_PI * pow(2,$this->dimensions['radius']);
    }
}

class Box extends Figure
{
    public function __construct($size)
    {
        $this->dimensions['size'] = $size;
    }

    public function getSquare()
    {
        return $this->dimensions['size'];
    }
}

class Triangle extends Figure
{
    public function __construct($dimensions)
    {
        $this->dimensions = $dimensions;
    }

    public function getSquare()
    {
        return $this->dimensions['height'];
    }

}
$circle = new Circle(5);
echo $circle->getSquare() . PHP_EOL;

$circle = new Box(5);
echo  $circle->getSquare() . PHP_EOL;

$triangle = new Triangle(['height' => 65]);
echo $triangle->getSquare() . PHP_EOL;

