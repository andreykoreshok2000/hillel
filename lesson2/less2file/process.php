<?php

interface NumericUnit
{
    /**
     * Get sum of numbers.
     *
     * @return mixed
     */
    public function sum();
}

interface StringUnit
{
    /**
     * Get concatenated strings.
     *
     * @return string
     */
    public function concatenate(): string;
}

class Numbers implements NumericUnit
{
    private $inputNumbers;

    public function __construct(array $inputNumbers)
    {
        $this->inputNumbers = $inputNumbers;
    }

    /**
     * @return float|int
     */
    public function sum()
    {
        return array_sum($this->inputNumbers);
    }
}

class Strings implements StringUnit
{
    private $inputStrings;

    public function __construct(array $inputStrings)
    {
        $this->inputStrings = $inputStrings;
    }

    /**
     * @return string
     */
    public function concatenate(): string
    {
        $string = '';

        foreach ($this->inputStrings as $item)
        {
            $string .= $item;
        }

        return $string;
    }
}

class UnitProcessor
{
    /**
     * @param NumericUnit|StringUnit $object
     */
    public static function process($object)
    {
        if ($object instanceof StringUnit) {
            return $object->concatenate();
        }

        if ($object instanceof NumericUnit) {
            return $object->sum();
        }
    }
}

$stringUnit = new Strings([
   'a', 'b', 'c'
]);

$numberUnit = new Numbers([
    1,2,3,4,5
]);

echo UnitProcessor::process($stringUnit) . PHP_EOL;
echo UnitProcessor::process($numberUnit). PHP_EOL;
