<?php

trait NameReverserTrait
{
    public function reverseName($name)
    {
        return strrev($name);
    }
}

trait NameToUpperTrait
{
    public function upperName($name)
    {
        return strtoupper($name);
    }
}

class User
{
    use NameReverserTrait;

    private $name;

    public function setName($name)
    {
        $this->name = $this->reverseName($name);
    }

    public function getName()
    {
        return $this->name;
    }
}

class AdminUser
{
    use NameReverserTrait, NameToUpperTrait;

    private $name;

    public function setName($name)
    {
        $this->name = $this->reverseName($this->upperName($name));
    }

    public function getName()
    {
        return $this->name;
    }
}

$user = new User();
$user->setName('Vasia');
echo $user->getName(). PHP_EOL;

$adminUser = new AdminUser();
$adminUser->setName('VasiaAdmin');
echo $adminUser->getName(). PHP_EOL;