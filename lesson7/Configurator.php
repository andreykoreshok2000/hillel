<?php

class Base
{
    protected $settings;
    protected $message;

    public function setConnection($settings)
    {
        $this->settings = $settings;
    }

    public function getMessage()
    {
        return $this->message;
    }

}

interface ConfiguratorInterface
{
    public function configure();
}

class MailConfigurator extends Base implements ConfiguratorInterface
{

    private $configuration;

    public function configure()
    {
        $this->configuration = $this->settings['mailer_options'];
        return $this;
    }
}

class DatabaseConfigurator extends Base implements ConfiguratorInterface
{

    private $configuration;

    public function configure()
    {
        $this->configuration['dsn'] = $this->settings['dsn'];
        $this->configuration['user'] = $this->settings['user'];
        $this->configuration['password'] = $this->settings['password'];
        return $this;
    }
}

class CacheConfigurator extends Base implements ConfiguratorInterface
{

    private $configuration;

    public function configure()
    {
        $this->configuration['host'] = $this->settings['host'];
        $this->configuration['port'] = $this->settings['poer'];
        $this->configuration['user'] = $this->settings['user'];
        $this->configuration['password'] = $this->settings['password'];
        return $this;
    }
}
