<?php

//Hint - Open Closed Principle
class AnotherProgrammer
{

    public function action()
    {
        return 'coding';
    }

}
class Tester
{

    public function action()
    {
        return 'testing';
    }

}

/** Что если добавить еще класс Designer с методом draw() **/

class ProjectManagement
{
    public function process($member)
    {
        if (is_object($member)) {
            $member->action();
        } else {
            throw new Exception('Invalid input member');
        }

    }
}

