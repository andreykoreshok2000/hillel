<?php

//Hint - Interface Segregation Principle
interface WorkTableInterface
{
    public function test();
}

interface codeInterface
{
    public function code();
}

interface canCodeInterface
{
    public function canCode();
}

class Programmer implements WorkTableInterface, codeInterface, canCodeInterface
{
    public function canCode()
    {
        return true;
    }
    public function code()
    {
        return 'coding';
    }
    public function test()
    {
        return 'testing in localhost';
    }
}

class Tester implements WorkTableInterface
{
    public function test()
    {
        return 'testing in test server';
    }
}

class ProjectManagement
{
    public function processCode($member)
    {
        if ($member->canCode()) {
            $member->code();
        }
    }
}
