<?php

//Hint - use Single Responsibility Principle Violation
class Report
{
    public function getTitle()
    {
        return 'Report Title';
    }

    public function getDate()
    {
        return '2016-04-21';
    }

}

class Content
{

    public $title;
    public $date;

    public function __construct($report)
    {
        $this->title = $report->getTitle();
        $this->date = $report->getDate();

    }

    public function getContents()
    {
        return [
            'title' => $this->title,
            'date' => $this->date,
        ];
    }

    public function formatJson()
    {
        return json_encode($this->getContents());
    }

}
