<?php

//Hint - Dependency Inversion Principle
class Mailer
{
}

class SendWelcomeMessage
{
    private $mail;

    public function __construct($mail)
    {
        $this->mail = $mail;
    }
}

//SendWelcomeGoolge
//SendWelcomeSendgrid
//SendWelcomeMailchimp
