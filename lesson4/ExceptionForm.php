
<form>
    <label for="string">string:</label><br>
    <input type="text" id="string" name="string" value=""><br>
    <label for="integer">integer:</label><br>
    <input type="text" id="integer" name="integer" value=""><br><br>
    <input type="submit" value="Submit">
</form>

<?php

class EmptyStringException extends Exception
{
    private $input;

    public function __construct($input, $message = "", $code = 0, Exception $previous = null)
    {
        $this->input = $input;
        $message = 'field string is empty';
        parent::__construct($message, $code, $previous);
    }

    public function getInput()
    {
        return $this->input;
    }

}

class InvalidInputTypeException extends Exception
{
    private $input;

    public function __construct($input, $message = "", $code = 0, Exception $previous = null)
    {
        $this->input = $input;
        $message = 'field integer not string';
        parent::__construct($message, $code, $previous);
    }

    public function getInput()
    {
        return $this->input;
    }

}

class ExceptionForm
{
    private $string;
    private $integer;

    public function __construct($get)
    {
        $this->string = $get['string'];
        $this->integer = $get['integer'];
    }

    public function formValid(): void
    {
        if (empty($this->string)) {
            throw new EmptyStringException($this->string);
        }

        if (!is_string($this->integer)) {
            throw new InvalidInputTypeException($this->integer);
        }
    }

}

if (!empty($_GET))
{
    $formValid = new ExceptionForm($_GET);
    try{
        $formValid->formValid();
    }catch (Exception $e) {
        echo $e->getMessage();
    }
}



