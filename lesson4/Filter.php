<?php

class Filter
{
    public $arr = [];
    public $numbers = [];
    public $string = [];

    public function __construct($arr)
    {
        if (!empty($arr))
        {
            $this->arr = $arr;
        }
    }

    public function getNumbers()
    {

        foreach ($this->arr as $key)
        {
            if (is_numeric($key))
            {
                $this->numbers[] += $key;
            }
        }
        print_r($this->numbers);
    }

    public function getStrings()
    {

        foreach ($this->arr as $key)
        {
            if (is_string($key))
            {
                $this->string[] = $key;
            }
        }
        print_r($this->string);
    }

}

$filter = new Filter(['f', 2, 't', 7, 2, 'k']);
$filter->getNumbers(); //[2,7,2]
$filter->getStrings(); // ['f', 't', 'k']

