<?php

class Concatenator
{

    public static function __callStatic($name, $arguments)
    {
        if (!empty($arguments))
        {
            return str_replace(' ', '+', strtolower(implode($arguments)));
        }
        return null;
    }
}

$concatenated = Concatenator::prepareString('I am concatenated');

echo $concatenated; // i+am+concatenated