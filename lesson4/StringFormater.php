<?php

class StringFormater
{
    private $userName = [];

    public function __get($name)
    {
        if (!isset($name)){
            return null;
        }
        return $this->userName[$name];
    }

    public function __set($name, $value)
    {
        $this->userName[$name] = strtoupper($value);
    }
}

$stringFormater = new StringFormater();
$stringFormater->name = 'uSeRnaMe';
echo $stringFormater->name; // вывести USERNAME